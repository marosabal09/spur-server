FROM swiftdocker/swift

# RUN apt-get update && apt-get -y install curl && rm -rf /var/lib/apt/lists/*
# RUN curl -sL https://apt.vapor.sh | bash
# RUN apt-get update && apt-get -y install swift vapor && rm -rf /var/lib/apt/lists/*

WORKDIR /SpurServer
COPY Config Config
COPY Sources Sources
COPY Tests Tests
COPY Package.swift Package.swift

EXPOSE 8080

RUN swift build -c release

ENTRYPOINT [".build/release/Run", "--env=production"]