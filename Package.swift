// swift-tools-version:4.0
// Generated automatically by Perfect Assistant 2
// Date: 2018-04-23 16:53:29 +0000
import PackageDescription

let package = Package(
	name: "SpurServer",
	products: [
		.library(name: "App", targets: ["App"]),
		.executable(name: "Run", targets: ["Run"])
	],
	dependencies: [
		.package(url: "https://github.com/vapor/vapor.git", "2.2.0"..<"3.0.0"),
		.package(url: "https://github.com/vapor/leaf-provider.git", "1.1.0"..<"2.0.0")
	],
	targets: [
		.target(name: "App", dependencies: ["Vapor", "LeafProvider"]),
		.target(name: "Run", dependencies: ["App"]),
		.testTarget(name: "AppTests", dependencies: ["App", "Testing"])
	]
)